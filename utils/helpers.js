module.exports = {
    even: num => num % 2 !== 0,
    titleCase: text => text.split(/[\-_\s]/).map(item => item !== 'and' ? `${item.charAt(0).toUpperCase()}${item.slice(1)}` : item).join(' ')
};