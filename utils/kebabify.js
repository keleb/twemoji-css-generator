const MAP = [
    {
        chars: '&',
        replaceWith: 'and'
    },
    {
        chars: 'ç',
        replaceWith: 'c'
    },
    {
        chars: 'âäàáāåąã',
        replaceWith: 'a'
    },
    {
        chars: 'êëèéēę',
        replaceWith: 'e'
    },
    {
        chars: 'îïìíī',
        replaceWith: 'i'
    },
    {
        chars: 'ôöòóōøõ',
        replaceWith: 'o'
    },
    {
        chars: 'ûüùúū',
        replaceWith: 'u'
    },
    {
        chars: 'ñ',
        replaceWith: 'n'
    },
];

String.prototype.replaceMap = function (map) {
    text = this.valueOf();

    map.forEach(item => {
        text = text.replace(new RegExp(`[${item.chars}]`, 'g'), item.replaceWith);
    });

    return text;
};

module.exports = char => {
    char = char || '-';

    return function (data, customReplacer) {
        let replacer = char || customReplacer;

        return data.toLowerCase() 
        // Replace non word characters with empty spaces
        .replace(/['’"]/g, '')
        // Replace special symbols
        .replaceMap(MAP)
        // Replace non word characters with a dash
        .replace(/[ .,/#!$%^?–*;:{}=\-_`“”~()]/g, replacer)
        // Ensure we don't have multiple dashes next to each other
        .replace(new RegExp(`(${replacer}+)`, 'g'), replacer)
        // Ensure we don't have a leading or trailing dash
        .replace(new RegExp(`(${replacer}$)|(^${replacer})`, 'g'), '');
    }
}