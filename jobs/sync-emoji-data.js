const fs = require('fs');
const request = require('request');
const async = require('async');
const kebabify = require('../utils/kebabify')('_');
const codify = require('../utils/kebabify')('-');

let data = fs.readFileSync('./data/src/emojis.txt', 'utf8').split(/\n/);

data = data.filter(line => {
    return !!line[0] && !(line[0] === '#' && !(line.substring(0, 7) === '# group' || line.substring(0, 10) === '# subgroup'));
});

let emojiData = {};
let nameMapper = {};
let currentGroup;
let currentSubgroup;

async.forEachOfSeries(data, (line, key, next) => {
    if (line.substring(0, 7) === '# group') {
        groupRaw = line.replace('# group: ', '').trim();
        currentGroup = kebabify(groupRaw);
        console.log(`Getting group: ${groupRaw}`)
        nameMapper[currentGroup] = groupRaw;
        emojiData[currentGroup] = {};
        next(null);
    } else if (line.substring(0, 10) === '# subgroup') {
        subgroupRaw = line.replace('# subgroup: ', '').trim();
        currentSubgroup = kebabify(subgroupRaw);
        console.log(`Getting subgroup: ${subgroupRaw}`)
        nameMapper[currentSubgroup] = subgroupRaw;
        emojiData[currentGroup][currentSubgroup] = {};
        next(null);
    } else {
        let status = line.split(';')[1].trim().split(' ')[0];

        if (status === 'fully-qualified')

        let emojiAndDesc = line.split('#')[1].trim();
        let description = kebabify(emojiAndDesc).split('_').slice(3).join('_');
        let humanDescription = description.replace(/_/g, ' ');
        let className = `.twe--${codify(humanDescription)}`;
        let emoji = kebabify(emojiAndDesc).split('_').shift();
        let code = line.split(';')[0].trim();

        let url = `https://twemoji.maxcdn.com/2/svg/${codify(code)}.svg`;

        console.log(`Getting emoji: ${humanDescription}`);
        
        request(url, (err, response) => {
            if (err || response.statusCode !== 200) {
                url = '';
            }

            emojiData[currentGroup][currentSubgroup][codify(code)] = {
                code: code,
                twemoji: url,
                emoji: emoji, 
                description: humanDescription,
                className: className
            };

            next(null);
        });
    }
}, err => {
    if (err) return;
    console.log('Done! Writing to file...');

    fs.writeFileSync('./data/emoji-data.json', JSON.stringify({groups: emojiData, name_mapper: nameMapper}, null, 2));
});