const dotenv = require('dotenv');
dotenv.config();

const express = require('express');
const exphbs  = require('express-handlebars');
 
const app = express();
const port = process.env.PORT || 6969;
 
app.engine('hbs', exphbs({
    defaultLayout: 'main', 
    extname: '.hbs',
    helpers: require("./utils/helpers.js")
}));
app.set('view engine', 'hbs');
 
const data = require('./data/emoji-data.json');

app.use(express.static('dist'));

app.get('/', function (req, res) {
    res.redirect('/twemoji');
});

app.get('/twemoji', function (req, res) {
    res.render('index', data);
});
 
app.listen(port);
console.log(`Server listening on port: ${port}`);