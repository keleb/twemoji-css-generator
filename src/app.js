require('./app.scss');

const req = require.context('./js', true, /^(.*\.(js$))[^.]*$/);
req.keys().forEach(function (key) {
    req(key);
});

const fav = require.context('./favicon', true, /\.(png|ico)$/);
fav.keys().forEach(function (key) {
    fav(key);
});

const icons = require.context('./icons', true, /\.(svg)$/);
icons.keys().forEach(function (key) {
    icons(key);
});