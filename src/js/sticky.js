const $bar = $('#bar');
const $parent = $bar.parent();
const $barCopy = $bar.clone().attr('id', 'bar_copy').css({
    position: 'absolute',
    visibility: 'hidden',
    top: 0,
    left: 0
});

$parent.append($barCopy);

window.addEventListener('scroll', () => {
    const barOffset = $barCopy.offset();

    window.requestAnimationFrame(function () {
        if (window.scrollY >= barOffset.top) {
            $barCopy.css({
                position: 'relative'
            });
            $bar.css({
                position: 'fixed',
                top: 0,
                left: barOffset.left,
                width: barOffset.width
            }).addClass('sticky');
        } else {
            $barCopy.css({
                position: 'absolute'
            });
            $bar.css({
                position: 'relative',
                top: '',
                left: '',
                width: ''
            }).removeClass('sticky');;
        }
    });
});

window.addEventListener('resize', () => {
    const barOffset = $barCopy.offset();

    window.requestAnimationFrame(function () {
        if (window.scrollY >= barOffset.top) {
            $bar.css({
                left: barOffset.left,
                width: barOffset.width
            }).addClass('sticky');
        } 
    });
});