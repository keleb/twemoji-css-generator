const $searchCategories = $('#search_categories');

$emptyState = $('#empty_list');
$emptyCategories = $('#empty_categories');

$searchCategories.on('input', e => {
    const $el = $(e.target);

    const searchTerm = $el.val().toLowerCase();

    if (searchTerm) {
        $('.hyperlink__section').each(function () {
            const $section = $(this);

            const $matches = $section.find('.hyperlink').filter(function () {
                return $(this).text().toLowerCase().includes(searchTerm);
            });

            $section.find('.hyperlink--small').addClass();
            $matches.removeClass('hidden');

            if ($matches.length === 0) {
                $section.addClass('hidden');
            } else {
                $section.removeClass('hidden');
            }
        });

        if ($('.hyperlink__section:not(.hidden)').length > 0) {
            $emptyCategories.removeClass('active');
        } else {
            $emptyCategories.addClass('active');
        }
    } else {
        $('.hyperlink__section').removeClass('hidden');
        $('.hyperlink').removeClass('hidden');
    }
});

const $searchDescription = $('#search_description');

$searchDescription.on('input', e => {
    const $el = $(e.target);

    const searchTerm = $el.val().toLowerCase();

    if (searchTerm) {
        const $groups = $('.group');
        $('.emoji-table .row').addClass('hidden');

        $groups.each(function () {
            const $group = $(this);
            const $groupHeader = $group.children('.header');

            $group.find('.subgroup').each(function () {
                const $subgroup = $(this);
                const $subgroupHeader = $subgroup.children('.subheader');
                const $rows = $subgroup.find('.row:not(.subheader)');

                const $matches = $rows.filter(function () {
                    return $(this).find('.description').text().toLowerCase().includes(searchTerm);
                });

                if ($matches.length > 0) {
                    $subgroupHeader.removeClass('hidden');
                    $groupHeader.removeClass('hidden');
                }

                $matches.removeClass('hidden');
            })
        });

        if ($('.emoji-table .row:not(.hidden)').length > 0) {
            $emptyState.removeClass('active');
        } else {
            $emptyState.addClass('active')
        }
    } else {
        $('.emoji-table .row').removeClass('hidden');
    }
})