const dotenv = require('dotenv');
dotenv.config();

const path = require('path');
const fs = require('fs');
const _ = require('lodash');
const hbs = require('handlebars');
const helpers = require('./utils/helpers.js');
const minify = require('html-minifier').minify;

_.forEach(helpers, (helper, key) => {
    hbs.registerHelper(key, helper);
});

const main = hbs.compile(fs.readFileSync('./views/layouts/main.hbs').toString());
const index = hbs.compile(fs.readFileSync('./views/index.hbs').toString());
const data = require('./data/emoji-data.json');

let pageHTML = main({
    body: index(data)
});

pageHTML = minify(pageHTML, {
    removeAttributeQuotes: true,
    removeRedundantAttributes: true,
    removeComments: true,
    removeTagWhitespace: true,
    collapseWhitespace: true,
    minifyCSS: true,
    minifyJS: true
})

const dest = path.join(__dirname, 'dist');
if (!fs.existsSync(dest)){
    fs.mkdirSync(dest);
}

const filePath = path.join(dest, 'index.html');
fs.writeFileSync(filePath, pageHTML);
console.log(`Built site to ${filePath}`);